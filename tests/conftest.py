import pytest
from selenium.webdriver import Chrome
from webdriver_manager.chrome import ChromeDriverManager

BASE_URL = "https://www.zagat.com"  # set the base url


@pytest.fixture()
def browser():
    # set the driver to the Chrome driver binary that is installed via our webdriver-manager package
    driver = Chrome(ChromeDriverManager().install())

    # wait for 5 seconds after launching the browser
    driver.implicitly_wait(5)

    # maximise window
    driver.maximize_window()

    yield driver  # below gets executed 'after' tests
    driver.quit()  # quit the driver

