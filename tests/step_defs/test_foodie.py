from urllib.parse import unquote

from pytest_bdd import (
    given,
    parsers,
    scenario, when, then)

from pages.base_page import BasePage
from pages.search_results_page import SearchResultsPage
from tests.conftest import BASE_URL


@scenario('../features/find_a_restaurant.feature', 'Searching for all restaurants in San Diego')
def test_searching_for_all_restaurants_in_san_diego():
    """Searching for all restaurants in San Diego"""


@given(parsers.cfparse('that {name} decides to use Zagat to find recommended restaurants'))
def that_frank_decides_to_use_zagat_to_find_recommended_restaurants(browser, name):
    browser.get(BASE_URL)


@when(parsers.cfparse('he searches for all restaurants in his area of "{area}"'))
def he_searches_for_all_restaurants_in_his_area_of_san_diego(browser, area):
    base_page = BasePage(browser)
    base_page.search_for_restaurants_in(area)


@then(parsers.cfparse('a list of recommended restaurants in "{area}" are returned to him'))
def a_list_of_recommended_restaurants_in_san_diego_are_returned_to_him(browser, area):
    assert area in unquote(browser.current_url)
    search_results_page = SearchResultsPage(browser)
    assert search_results_page.get_returned_search_results().is_displayed()
