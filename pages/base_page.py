from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys


class BasePage:
    __INITIAL_SEARCH_FIELD = (By.CLASS_NAME, 'zgt-header-search-text')
    __LOCATION_SEARCH_FIELD = (By.CLASS_NAME, 'zgt-search-bar-location-term-input')

    def __init__(self, browser):
        self.browser = browser

    def search_for_restaurants_in(self, area):
        self.__get_initial_search_field().click()
        self.__set_location__search_field(area)

    def __get_initial_search_field(self):
        return self.browser.find_element(*self.__INITIAL_SEARCH_FIELD)

    def __get_location_search_field(self):
        return self.browser.find_element(*self.__LOCATION_SEARCH_FIELD)

    def __set_location__search_field(self, area):
        self.__get_location_search_field().clear()
        self.__get_location_search_field().send_keys(area + Keys.RETURN)
