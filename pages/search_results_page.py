from selenium.webdriver.common.by import By

from pages.base_page import BasePage


class SearchResultsPage(BasePage):
    RETURNED_SEARCH_RESULTS = (By.CLASS_NAME, 'zgt-place-results-list')

    def get_returned_search_results(self):
        return self.browser.find_element(*self.RETURNED_SEARCH_RESULTS)
